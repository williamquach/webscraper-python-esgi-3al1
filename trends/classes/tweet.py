import datetime
from .user import User

class Tweet:
    def __init__(self, user, date, content, link, interactions):
        self.user = user
        self.date = date
        self.content = content
        self.link = link
        self.__interactions = interactions

    def get_user(self):
        return self.__user

    def get_interactions(self):
        return self.__interactions

    def get_date(self):
        return self.__date

    def get_content(self):
        return self.__content

    def get_link(self):
        return self.__link

    def set_user(self, user):
        if type(user) is User and user is not None:
            self.__user = user
        else:
            raise AttributeError("Cannot set tweet user")

    def set_date(self, date):
        if type(date) is str:
            self.__date = date
        elif type(date) is datetime.datetime and date is not None:
            self.__date = date
        else:
            raise AttributeError("Cannot set tweet date")

    def set_content(self, content):
        print("content")
        print(content)
        print(type(content))
        if type(content) is str and content != '':
            self.__content = content
        else:
            raise AttributeError("Cannot set tweet content")

    def set_link(self, link):
        if type(link) is str and link != '' and 'http' in link and 'twitter.com' in link:
            self.__link = link
        else:
            raise AttributeError("Cannot set tweet link")

    user = property(get_user, set_user)
    date = property(get_date, set_date)
    content = property(get_content, set_content)
    link = property(get_link, set_link)
    interactions = property(get_interactions)

    def __str__(self):
        return f"{self.user.name} - @{self.user.userName} ({self.link}):\n {self.content}"
