class TrendFilters:
    def __init__(self, filteringByCategory, filteringByNbTweetsMin, filteringByNbTweetsMax, search):
        self.filteringByCategory = filteringByCategory
        self.filteringByNbTweetsMin = filteringByNbTweetsMin
        self.filteringByNbTweetsMax = filteringByNbTweetsMax
        self.search = search

    def get_Category(self):
        return self.__category

    def get_NbTweetsMin(self):
        return self.__nbTweetsMin

    def get_NbTweetsMax(self):
        return self.__nbTweetsMax

    def get_Search(self):
        return self.__search

    def set_Category(self, category):
        if type(category) is str:
            self.__category = category
        else:
            raise AttributeError(f"Cannot set filtering category ({category})")

    def set_Search(self, searchingStr):
        if type(searchingStr) is str:
            self.__search = searchingStr
        else:
            raise AttributeError(f"Cannot set filtering searchTrend ({searchingStr})")

    def set_NbTweetsMin(self, nbTweetsMin):
        if nbTweetsMin == '' or nbTweetsMin is None:
            self.__nbTweetsMin = None
        elif type(nbTweetsMin) is int or str(nbTweetsMin).isdigit():
            self.__nbTweetsMin = int(nbTweetsMin)
        else:
            raise AttributeError(f"Cannot set filtering nbTweetsMin ({nbTweetsMin})")

    def set_NbTweetsMax(self, nbTweetsMax):
        if nbTweetsMax == '' or nbTweetsMax is None:
            self.__nbTweetsMax = None
        elif type(nbTweetsMax) is int or str(nbTweetsMax).isdigit():
            self.__nbTweetsMax = int(nbTweetsMax)
        else:
            raise AttributeError(f"Cannot set filtering nbTweetsMax ({nbTweetsMax})")

    filteringByCategory = property(get_Category, set_Category)
    filteringByNbTweetsMin = property(get_NbTweetsMin, set_NbTweetsMin)
    filteringByNbTweetsMax = property(get_NbTweetsMax, set_NbTweetsMax)
    search = property(get_Search, set_Search)

    def hasFilters(self):
        return self.filteringByCategory != "" or self.filteringByNbTweetsMin is not None or self.filteringByNbTweetsMax is not None or self.search != ""

    def __str__(self):
        return f"Filters : \n" \
               f"- Category = {self.filteringByCategory}\n" \
               f"- NbTweetsMin = {self.filteringByNbTweetsMin}\n" \
               f"- NbTweetsMax = {self.filteringByNbTweetsMax}\n" \
               f"- Search = {self.search}\n"
