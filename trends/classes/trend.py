import urllib.parse

def human_format(num):
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    # add more suffixes if you need them
    if num - abs(num) == 0:
        return '%.0f%s' % (num, ['', 'K', 'M', 'G', 'T', 'P'][magnitude])
    return '%.2f%s' % (num, ['', 'K', 'M', 'G', 'T', 'P'][magnitude])

class Trend:
    def __init__(self, category, name, nbTweets):
        self.__nbTweetsFormatted = None
        self.__nbTweets = None
        self.__url = ""

        self.category = category
        self.name = name
        self.nbTweets = nbTweets
        self.url = self.name

    def get_category(self):
        return self.__category

    def get_name(self):
        return self.__name

    def get_nbTweets(self):
        return self.__nbTweets

    def get_nbTweetsFormatted(self):
        return self.__nbTweetsFormatted

    def get_url(self):
        return self.__url

    def set_category(self, category):
        if type(category) is str and category != '':
            self.__category = category
        else:
            raise AttributeError(f"Cannot set trend category ({category})")

    def set_name(self, name):
        if type(name) is str and name != '':
            self.__name = name
        else:
            raise AttributeError(f"Cannot set trend name ({name})")

    def set_url(self, name):
        if type(name) is str and name != '':
            twitterSearchUrl = 'https://www.twitter.com/search?q='
            twitterSearchUrl += urllib.parse.quote(name)
            twitterSearchUrl += '&src=trend_click&vertical=trends'

            self.__url = twitterSearchUrl
            print(self.__url)
        else:
            raise AttributeError(f"Cannot set trend name ({name})")

    def set_nbTweets(self, nbTweets):
        if nbTweets is None:
            self.__nbTweets = None
            return

        nbTweets = str(nbTweets).replace(' ', '').replace('Tweets', '').strip()
        if nbTweets is str and nbTweets.isdigit():
            nbTweets = int(nbTweets)
            if nbTweets <= 0:
                self.__nbTweets = None
            else:
                self.__nbTweets = nbTweets
        elif 'k' in nbTweets or 'K' in nbTweets:
            nbTweets = float(str(nbTweets).replace('k', '').replace('M', '').replace(',', '.'))
            self.__nbTweets = int(nbTweets * 1000)
        elif 'M' in nbTweets or 'm' in nbTweets:
            nbTweets = float(str(nbTweets).replace('m', '').replace('M', '').replace(',', '.'))
            self.__nbTweets = int(nbTweets * 1000000)
        elif type(nbTweets) is str:
            nbTweets = "".join(nbTweets.split())
            try:
                self.__nbTweets = int(nbTweets)
            except ValueError:
                self.__nbTweets = None
                print(f"Error converting str to int with : {nbTweets}")
        else:
            raise AttributeError(f"Cannot set trend nb tweets ({nbTweets})")
        self.nbTweetsFormatted = self.__nbTweets

    def set_nbTweetsFormatted(self, nbTweetsFormatted):
        if nbTweetsFormatted is None:
            self.__nbTweetsFormatted = None
        elif type(nbTweetsFormatted) is int:
            if nbTweetsFormatted == 0:
                self.__nbTweetsFormatted = 0
            else:
                self.__nbTweetsFormatted = human_format(nbTweetsFormatted)
        else:
            raise AttributeError(f"Cannot set trend nb tweets formatted ({nbTweetsFormatted})")

    category = property(get_category, set_category)
    name = property(get_name, set_name)
    url = property(get_url, set_url)
    nbTweets = property(get_nbTweets, set_nbTweets)
    nbTweetsFormatted = property(get_nbTweetsFormatted, set_nbTweetsFormatted)

    def __str__(self):
        tweets = "unkn0wn" if self.nbTweets is None else f"{self.nbTweets} tweets"
        return f"{self.category} : {self.name} ({tweets})"
