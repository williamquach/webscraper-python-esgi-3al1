class User:

    def __init__(self, name, userName):
        self.name = name
        self.userName = userName
        self.link = 'https://twitter.com/' + userName

    def get_name(self):
        return self.__name

    def get_link(self):
        return self.__link

    def get_userName(self):
        return self.__userName

    def set_name(self, name):
        if type(name) is str and name != '':
            self.__name = name
        else:
            raise AttributeError("Cannot set user's name")

    def set_link(self, link):
        if type(link) is str and link != '' and 'http' in link and 'twitter.com' in link:
            self.__link = link
        else:
            raise AttributeError("Cannot set user's link")

    def set_userName(self, userName):
        if type(userName) is str and userName != '':
            self.__userName = userName
        else:
            raise AttributeError("Cannot set user's username")

    name = property(get_name, set_name)
    link = property(get_link, set_link)
    userName = property(get_userName, set_userName)

    def __str__(self):
        return f"{self.name} - @{self.userName} (url : {self.link})"
