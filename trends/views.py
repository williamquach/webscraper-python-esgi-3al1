from django.shortcuts import render
from django.http import HttpResponse
from .functions import *

# Create your views here.

def index(request):
    # WebScraping
    trends = getTwitterTrends()
    context = {'trends': trends}
    return render(request, 'trends/index.html', context)

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)


def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)
