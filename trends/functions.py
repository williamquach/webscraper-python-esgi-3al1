from time import sleep
from bs4 import BeautifulSoup
import requests
from selenium import webdriver
from selenium.webdriver import FirefoxOptions
from .classes.trend import Trend
import datetime
from .classes.trend_filters import TrendFilters


def getTwitterTrends():
    url = 'https://twitter.com/i/trends'
    # Settings for driver
    profile = webdriver.FirefoxProfile()
    profile.update_preferences()
    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(firefox_profile=profile, options=options)

    # Use selenium to fake accessing to url
    driver.get(url)
    sleep(3)  # Wait for content to load

    # Retrieve HTML
    html = driver.page_source
    driver.quit()
    soup = getCleanHtmlContent(html)

    # => Here we want to get tendances / current trends
    # Getting all div of trends
    divTrends = soup.findAll('div', {'class': 'css-1dbjc4n r-16y2uox r-bnwqim'})

    trends = list()
    # Parcouring trends to parse them
    for divTrend in divTrends:
        trendCategory = "Trend default category"
        trendNameDiv = "Trend default name"
        trendNbTweetsDiv = None

        trendCategorySpan = divTrend \
            .find('div', {'class': 'css-901oao r-9ilb82 r-1qd0xha r-n6v787 r-16dba41 r-1cwl3u0 r-bcqeeo r-qvutc0'}) \
            .find('span')

        if hasMethod(trendCategorySpan, 'string'):
            trendCategory = trendCategorySpan.string

        trendNameDivSpan = divTrend \
            .find('div',
                  {
                      'class': 'css-901oao r-1fmj7o5 r-1qd0xha r-a023e6 r-b88u0q r-rjixqe r-bcqeeo r-vmopo1 r-qvutc0'}) \
            .find('span')

        if hasMethod(trendNameDivSpan, 'string'):
            trendNameDiv = trendNameDivSpan.string

        if 'Tweets' in str(divTrend):
            trendNbTweetsDiv = divTrend \
                .find('div',
                      {
                          'class': 'css-901oao r-9ilb82 r-1qd0xha r-n6v787 r-16dba41 r-1cwl3u0 r-14gqq1x r-bcqeeo r-qvutc0'}) \
                .find('span') \
                .string

        trend = Trend(str(trendCategory), str(trendNameDiv), trendNbTweetsDiv)
        print(trend)
        trends.append(trend)

    return trends


def hasMethod(anObject, method):
    if hasattr(anObject, method):
        return True
    return False


def getSoupHtmlContentByUrl(url):
    """Get html content without script, style & iframe elements"""
    r = requests.get(url)
    html = r.text
    return getCleanHtmlContent(html)


def getCleanHtmlContent(html):
    """Get html content without script, style & iframe elements"""
    soup = BeautifulSoup(html, 'html.parser')
    [s.extract() for s in soup(['iframe', 'script', 'style'])]
    return soup


def scrapGoogleSearch():
    # WebScraping
    # HTTP REQUEST On site
    url = 'https://www.google.com/search?q=test'
    # Get content of site (TWITTER.COM, others...)
    soup = getSoupHtmlContentByUrl(url)
    print(soup.prettify())

    # print(soup.find_all('h3'))

    buttonContinue = soup.findAll('input', {"name": "continue"})[0]
    if buttonContinue:
        urlContinue = buttonContinue['value']
        soup = getSoupHtmlContentByUrl(urlContinue)

    print(soup.prettify())
    # Get informations in content


def getTrendsSortedAndFiltered(request):
    nowDate = datetime.datetime.now()
    allTrends = request.session.get('allTrends', list())
    twitterTrends = allTrends
    storeTrendsDate = request.session.get('storeTrendsDate', nowDate)
    dateDifference = nowDate - storeTrendsDate

    shouldReloadAllTrends = request.POST.get("reloadAllTrends", "") != ""

    # Trends are not set or were stored more than 30 min ago
    if len(allTrends) == 0 or dateDifference.seconds > 1800 or shouldReloadAllTrends:
        allTrends = getTwitterTrends()
        request.session['storeTrendsDate'] = nowDate
        request.session['allTrends'] = allTrends
        print("Reloaded twitter trends")

    if shouldReloadAllTrends:
        return {
            "twitterTrends": allTrends,
            'sortingDirection': 'DESC',
            'sortingCriteria': '',
            'filteringByCategory': '',
            'filteringByNbTweetsMin': '',
            'filteringByNbTweetsMax': '',
            'search': '',

            'shouldFilter': False,
            'shouldSort': False
        }

    # Retrieve sorting & filtering criterias
    sortingCriteria = request.POST.get('sortingCriteria', 'trendNbTweets')
    if sortingCriteria == '':
        # Basic sort
        print("Basic sort 1")
        sortingCriteria = 'trendNbTweets'
    if sortingCriteria == 'trendNbTweets':
        # Basic sort
        print("Basic sort 2")
        sortingDirection = request.POST.get('sortingDirection', 'DESC')
    else:
        sortingDirection = request.POST.get('sortingDirection', 'ASC')

    trendsFilter = getTrendFilters(request)
    filteringByCategory = trendsFilter.filteringByCategory
    filteringByNbTweetsMin = trendsFilter.filteringByNbTweetsMin
    filteringByNbTweetsMax = trendsFilter.filteringByNbTweetsMax
    search = trendsFilter.search

    # Filter
    shouldFilter = request.POST.get('shouldFilter', 'off') == 'on'
    if shouldFilter and trendsFilter and trendsFilter.hasFilters():
        print("FILTERING")
        if filteringByCategory != "" and trendsFilter.filteringByCategory is not None:
            print("Filtering category", filteringByCategory)
            twitterTrends = [trend for trend in twitterTrends if str(trend.category).lower() == str(filteringByCategory).lower()]

        if filteringByNbTweetsMin is not None:
            twitterTrends = [trend for trend in twitterTrends if
                             trend.nbTweets is None or trend.nbTweets >= int(filteringByNbTweetsMin)]

        if filteringByNbTweetsMax is not None:
            twitterTrends = [trend for trend in twitterTrends if
                             trend.nbTweets is None or trend.nbTweets <= int(filteringByNbTweetsMax)]

        if search != "" and trendsFilter.search is not None:
            twitterTrends = [
                trend
                for trend in twitterTrends
                if str(search).lower() in str(trend.name).lower() or
                str(trendsFilter.search).lower() in str(trend.category).lower() or
                (trend.nbTweetsFormatted and str(trendsFilter.search).lower() in str(trend.nbTweetsFormatted).lower())
            ]
    else:
        twitterTrends = allTrends

    # Set sort criteria
    trendCriteriaSortedBy = 'nbTweets'
    shouldSort = request.POST.get('shouldSort', 'off') == 'on'
    if shouldSort and sortingCriteria != '':
        print("SORTING")
        if sortingCriteria == 'trendName':
            trendCriteriaSortedBy = 'name'
        elif sortingCriteria == 'trendNbTweets':
            trendCriteriaSortedBy = 'nbTweets'
        elif sortingCriteria == 'trendCategory':
            trendCriteriaSortedBy = 'category'
        else:
            raise Exception(f"sortingCriteria is unknown when sorting trends {sortingCriteria}")

    # SORT
    twitterTrends.sort(key=lambda trend: (
        getattr(trend, trendCriteriaSortedBy).lower()
        if type(getattr(trend, trendCriteriaSortedBy)) is str
        else 0 if getattr(trend, trendCriteriaSortedBy) is None
        else getattr(trend, trendCriteriaSortedBy)
    ), reverse=True if sortingDirection == 'DESC' else False)

    request.session['trends'] = twitterTrends
    return {
        "twitterTrends": twitterTrends,
        'sortingDirection': 'DESC' if sortingDirection == 'ASC' else 'ASC',
        'sortingCriteria': sortingCriteria,
        'filteringByCategory': filteringByCategory,
        'filteringByNbTweetsMin': filteringByNbTweetsMin,
        'filteringByNbTweetsMax': filteringByNbTweetsMax,
        'search': search,

        'shouldFilter': shouldFilter,
        'shouldSort': shouldSort
    }


def getTrendCategories(twitterTrends):
    categories = list()
    for trend in twitterTrends:
        trendCategory = trend.category
        if trendCategory not in categories:
            categories.append(trendCategory)

    return categories


def getTrendFilters(request):
    filteringByCategory = request.POST.get('filteringByCategory', '')
    filteringByNbTweetsMin = request.POST.get('filteringByNbTweetsMin', None)
    filteringByNbTweetsMax = request.POST.get('filteringByNbTweetsMax', None)
    searchTrend = request.POST.get('searchTrend', '')
    return TrendFilters(filteringByCategory, filteringByNbTweetsMin, filteringByNbTweetsMax, searchTrend)
