import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import FirefoxOptions
from time import sleep
from trends.classes.user import *
from trends.classes.tweet import *
from _datetime import datetime

def getCleanHtmlContent(html):
    """Get html content without script, style & iframe elements"""
    soup = BeautifulSoup(html, 'html.parser')
    [s.extract() for s in soup(['iframe', 'script', 'style'])]
    return soup

def scrapSearchTwitter(request, search):
    searchResults = request.session.get('searchResults', list())
    oldSearch = request.session.get('oldSearch', list())
    if oldSearch == search and searchResults:
        return searchResults

    url = 'https://twitter.com/search?q=' + search + '&src=typed_query'
    # Settings for driver
    profile = webdriver.FirefoxProfile()
    profile.update_preferences()
    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(firefox_profile=profile, options=options)

    # Use selenium to fake accessing to url
    driver.get(url)
    sleep(3)  # Wait for content to load

    # Retrieve HTML
    html = driver.page_source
    driver.quit()
    soup = getCleanHtmlContent(html)

    divTwitts = soup.findAll('div', {'class': 'css-1dbjc4n r-1iusvr4 r-16y2uox r-1777fci r-kzbkwu'})

    tweets = list()

    for divTwitt in divTwitts:
        divUser = divTwitt.find('div', {'class': 'css-1dbjc4n r-1wbh5a2 r-dnmrzs'})
        spanUserName = divUser.find('span', {'class': 'css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0'})
        divUserUserName = divUser\
            .find('div', {'class': 'css-901oao css-bfa6kz r-9ilb82 r-18u37iz r-1qd0xha r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-qvutc0'}) \
            .find('span')
        user = User(str(spanUserName.string), str(divUserUserName.string))

        divTweetContent = divTwitt.find('div', {'class': 'css-901oao r-1fmj7o5 r-1qd0xha r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-bnwqim r-qvutc0'})
        # tweets.append(divTweetContent)
        # print("divTwitt")
        # print(divTwitt.prettify())
        aHrefDatetime = divTwitt('a', {'class': 'css-4rbku5 css-18t94o4 css-901oao r-9ilb82 r-1loqt21 r-1q142lx r-1qd0xha r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-3s2u2q r-qvutc0'})
        aHrefDatetime = aHrefDatetime[0]

        tweetLink = 'https://www.twitter.com/' + aHrefDatetime['href']
        strDatetime = str(aHrefDatetime.find('time')['datetime']).replace('T', ' ').replace('Z', '')
        tweetDate = datetime.strptime(strDatetime, '%Y-%m-%d %H:%M:%S.%f')
        spansTweetContent = divTweetContent.find('span', {'class': 'css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0'})
        tweetContent = ' '.join([span.string for span in spansTweetContent])
        tweetSmileys = divTweetContent.find('img')
        if tweetSmileys:
            tweetContent += str(' ' + tweetSmileys['alt'])

        interactions = divTwitt.find('div', {'class': 'css-1dbjc4n r-18u37iz r-1wtj0ep r-1s2bzr4 r-1mdbhws'})["aria-label"]

        tweet = Tweet(user, tweetDate, tweetContent, tweetLink, interactions=interactions)
        print(tweet)
        tweets.append(tweet)

    request.session['searchResults'] = tweets
    request.session['oldSearch'] = search

    return tweets


def getTwitterResults(request):
    twitterKeyword = request.POST.get("twitterKeyword", "")
    if twitterKeyword == "":
        return []
    twitts = scrapSearchTwitter(request, twitterKeyword)
    return twitts

    # TODO