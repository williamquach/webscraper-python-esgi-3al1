function loadTrendInSearchBar(trendName) {
    let globalSearch = document.getElementById('globalSearch');
    globalSearch.value = trendName;
    let twitterSearch = document.getElementById('twitterKeyword');
    twitterSearch.value = trendName;
}

function hideOrShowFiltering(element) {
    let tableFilter = document.getElementById(element);
    tableFilter.hidden = !tableFilter.hidden;
}