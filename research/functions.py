import requests
from bs4 import BeautifulSoup

websites = [
    "https://realpython.com/beautiful-soup-web-scraper-python",
    'https://docs.djangoproject.com/fr/3.2/intro/tutorial03/'
]

dont_contain_any = list("")
contain_atleast_one = list("")
contain_all = list("")
final = list("")

user_choices = [
    "Doesn't contain",
    "Contains at least",
    "Contains all"
]

def getResults(request):
    oldKeyWords = []

    choices = request.POST.getlist("choice")

    postKeywords = request.POST.get("keywords", "")
    postKeywords_any = request.POST.get("Doesn't contain", "")
    postKeywords_oneplus = request.POST.get("Contains at least", "")
    postKeywords_all = request.POST.get("Contains all", "")

    if len(choices) <= 0 and postKeywords == "" and postKeywords_any == "" and postKeywords_oneplus == "" and postKeywords_all == "":
        return {
            'user_choices': user_choices,
            'allResults': [],
            'dontContainAny': [],
            'containAtleastOne': [],
            'containAll': [],
            'keyWords': []
        }

    keywords = postKeywords.split()
    keywords_any = postKeywords_any.split()
    keywords_oneplus = postKeywords_oneplus.split()
    keywords_all = postKeywords_all.split()

    shouldNormalSearch = (len(keywords_any) + len(keywords_oneplus) + len(keywords_all)) <= 0
    # recherche normale
    if shouldNormalSearch:
        for website in websites:
            if get_contain_atleast_one(urlToString(website), keywords):
                final.append(website)

    # recherche avancée
    else:
        fillTables(keywords_any, keywords_oneplus, keywords_all)
        if "Contains at least" in choices:
            add(contain_atleast_one)
        if "Contains all" in choices:
            add(contain_all)
            for site in final:
                if site not in contain_all:
                    final.remove(site)
        if "Doesn't contain" in choices:
            add(dont_contain_any)
            for site in final:
                if site not in dont_contain_any:
                    final.remove(site)

    context = {'allResults': final, 'user_choices': user_choices, 'dontContainAny': keywords_any,
               'containAtleastOne': keywords_oneplus, 'containAll': keywords_all, 'keyWords': keywords}
    return context


def add(aList):
    final.clear()
    for link in aList:
        if link not in final:
            final.append(link)

def urlToString(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    script = soup.find('body')
    return str(script)


def get_dont_contain_any(stringURL, words):
    find = 0
    for word in words:
        if word.lower() in stringURL.lower():
            find += 1
    return find == 0


def get_contain_atleast_one(stringURL, words):
    find = 0
    for word in words:
        if word.lower() in stringURL.lower():
            find += 1
    return find >= 1


def get_contain_all(stringURL, words):
    find = 0
    for word in words:
        if word.lower() in stringURL.lower():
            find += 1
    return find == len(words)


def fillTables(keywords_any, keywords_oneplus, keywords_all):
    dont_contain_any.clear()
    contain_atleast_one.clear()
    contain_all.clear()
    for website in websites:
        if get_dont_contain_any(urlToString(website), keywords_any):
            dont_contain_any.append(website)
    for website in websites:
        if get_contain_atleast_one(urlToString(website), keywords_oneplus):
            contain_atleast_one.append(website)
    for website in websites:
        if get_contain_all(urlToString(website), keywords_all):
            contain_all.append(website)
