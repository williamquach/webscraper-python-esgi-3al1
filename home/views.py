from django.shortcuts import render
from trends.functions import *
from research.functions import *
from twittersearch.functions import *

# Create your views here.

def index(request):
    trendsContext = getTrendsSortedAndFiltered(request)
    trendsCategories = getTrendCategories(trendsContext.get("twitterTrends", []))

    print("Request method is POST")
    print("Parameters", request.POST)
    if request.method == 'POST':
        searchResults = getResults(request)
        twitterResults = getTwitterResults(request)
    else:
        searchResults = []
        twitterResults = []

    context = {
        'trends': trendsContext,
        'trendsCategories': trendsCategories,

        'results': searchResults,
        'twitterResults': twitterResults,
        'twitterSearched': request.POST.get('twitterKeyword', '')
    }
    return render(request, 'home/index.html', context)
